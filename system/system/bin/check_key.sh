#!/system/bin/sh
PATH=/system/bin/:$PATH

export_check_persist() {
	setprop vendor.asus.persist_data_list.status 1
	sleep 5
	ls_persist=`cat /asdf/persist_data_directory.txt`
	setprop vendor.asus.persist_data_list.directory  $ls_persist
	setprop vendor.asus.persist_data_list.status 0
}
export_check_key() {
	#am start -S -n com.asus.atd.deviceproperty/.MainActivity -e getinfo "AttesetationKey"
	am start-foreground-service -n com.asus.key_status/.A_KEY
	#am startservice -n com.asus.key_status/.A_KEY
	sleep 20

	akey_status=`getprop sys.asus.attk.status`
	echo "[AKEY]: check ${akey_status}" > /proc/asusevtlog

	if [ "${akey_status}" = "FALSE" ]; then
		echo "[rkp_reinstall][setprop vendor.asus.system.get.deviceid.status=1]: resintall rkp" > /proc/asusevtlog
		setprop vendor.asus.system.get.deviceid.status 1
	fi
	am force-stop com.asus.key_status
	echo "[AKEY]: check done!" > /proc/asusevtlog
}

export_check_persist
echo "[AKEY]: boot complete" > /proc/asusevtlog
sleep 10m
echo "[AKEY]: start" > /proc/asusevtlog

count="1"
while [ 1 ]
do
echo $count
count=$(($count+1))
export_check_persist
export_check_key
sleep 12h
done
