## qssi-user 14 UKQ1.230917.001 34.1004.0204.128 release-keys
- Manufacturer: asus
- Platform: kalama
- Codename: ASUS_AI2302
- Brand: asus
- Flavor: qssi-user
- Release Version: 14
- Kernel Version: 5.15.94
- Id: UKQ1.230917.001
- Incremental: 34.1004.0204.128
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: asus/WW_AI2302/ASUS_AI2302:13/TKQ1.220928.001/33.0204.0204.36:user/release-keys
- OTA version: 
- Branch: qssi-user-14-UKQ1.230917.001-34.1004.0204.128-release-keys
- Repo: asus/ASUS_AI2302
