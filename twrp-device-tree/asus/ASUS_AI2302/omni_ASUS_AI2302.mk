#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from ASUS_AI2302 device
$(call inherit-product, device/asus/ASUS_AI2302/device.mk)

PRODUCT_DEVICE := ASUS_AI2302
PRODUCT_NAME := omni_ASUS_AI2302
PRODUCT_BRAND := asus
PRODUCT_MODEL := ASUS_AI2302
PRODUCT_MANUFACTURER := asus

PRODUCT_GMS_CLIENTID_BASE := android-asus

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="WW_AI2302-user 13 TKQ1.220928.001 33.0204.0204.36 release-keys"

BUILD_FINGERPRINT := asus/WW_AI2302/ASUS_AI2302:13/TKQ1.220928.001/33.0204.0204.36:user/release-keys
